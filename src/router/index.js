import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/views/Login'
import Home from '@/views/Home'
import NotFound from '@/views/404'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path:'/login',
      name: 'Login',
      component: Login
    },
    {
      path:'/404',
      name: 'notFound',
      component: NotFound
    }
  ]
});

router.beforeEach((to,from,next)=>{
  let user =  sessionStorage.getItem('user')
  if (to.path == '/login'){
    // 如果是访问登录界面，如果会话信息存在  代表登录过 跳转到主页
    if(user){
      next({path:'/'})
    }else{
      next()
    }
  }else{
    // 如果用户访问非登录信息 且会话信息不存在 代表未登录  跳转到登录界面
    if(!user){
      next({path:'/login'})
    }else{
      next()
    }
  }
})

export default router
