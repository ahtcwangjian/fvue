import Mock from 'mockjs'

Mock.mock('http://localhost:8080/user', {
    data: {
        'name': '@name',
        'email': '@email',
        'age|1-10': 5
    }
})

Mock.mock('http://localhost:8080/menu', {
    data: {
        'id': '@increment',
        'name': 'menu',
        'order|10-20': 12
    }
})

Mock.mock('http://localhost:8080/login', {
    data: {
        'token': 'wangjian'
    }
})

